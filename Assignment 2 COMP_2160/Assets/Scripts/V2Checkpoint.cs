﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class V2Checkpoint : MonoBehaviour
{
    //HEALTH//
    public GameObject player;// GETS THE HEALTH
    public float healthBoost; // HEALTHBOOST VALUE
    private float maxPlayerHp; // PROVIDES THIS SCRIPT WITH A MAX PLAYER HEALTH VALUE
    private float playerHp; // PROVIDES THIS SCRIPT WITH A UPDATED PLAYER HEALTH VALUE
    //HEALTH//
    //CHECKPOINT HOLDER//
    public GameObject cManager; //GETS CHECKPOINT HOLDER
    //CHECKPOINT HOLDER//
    // Start is called before the first frame update
    void Start()
    {
        maxPlayerHp = player.GetComponent<Health_Player>().startPlayerHealth; //MAXPLAYERHP IS NOW LINKED TO HEALTH SCRIPT STARTPLAYERHEALTH
    }

    // Update is called once per frame
    void Update()
    {
        playerHp = player.GetComponent<Health_Player>().hp; // PLAYER HP IS NOW LINKED TO HEALTH PLAYER SCRIPT HP
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            cManager.GetComponent<V2Checkpoint_Manager>().trigger = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            cManager.GetComponent<V2Checkpoint_Manager>().checkpointIndex++;
            cManager.GetComponent<V2Checkpoint_Manager>().trigger = false;
            this.GetComponent<Renderer>().material.color = Color.red; // GETS THIS MATERIAL AND CHANGES ITS COLOR
            this.GetComponent<Light>().intensity = 0.0f; // TURNS OFF THE LIGHT
            this.GetComponent<Collider>().enabled = false; //DISABLES COLLIDER AFTER THE PLAYER EXITS
        }

        if(playerHp < maxPlayerHp)
        {
            player.GetComponent<Health_Player>().hp += healthBoost; // ON THE EXIT OF EACH CHECKPOINT THE PLAYER WILL RECIEVE A HEALTH BOOST
        }
    }
}
