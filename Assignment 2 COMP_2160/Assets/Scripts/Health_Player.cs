﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Health_Player : MonoBehaviour
{
    public Health_Bar healthBar; // GRABS THE HEALTH BAR

    public GameObject smoke; // GRABS THE SMOKE GAMEOBJECT

    public float smokeThreshold; // CREATES A VALUE THAT WILL BE THE SMOKE THRESHOLD

    public GameObject explosion; // GRABS THE EXPLOSION GAMEOBJECT

    public float startPlayerHealth = 100.0f; // DEFINES THE PLAYERS STARTING HEALTH

    public int damageThreshold = 2; // DEFINES THE DAMAGE THRESHOLD THAT MUST BE REACHED IN ORDER FOR THE PLAYER TO RECIEVE DAMAGE
    
    public float hp = 0.0f; // CREATES A HEALTH FLOAT

    public bool isDead; // CREATES A BOOL THAT DETERMINES IF THE PLAYER IS DEAD OR NOT

    public Collision otherOne;

    private int mag; // INT VALUE REPRESENTING MAGNITUDE
    // Start is called before the first frame update
    void Start()
    {
        hp = startPlayerHealth; // MAKES THE HP THE STARTING HEALTH
        healthBar.SetMaxHealth(startPlayerHealth); // MAKES THE HEALTHBAR VAULE OF HEATH THE START PLAYER HEALTH
        smokeThreshold = 30.0f; // DEFINES THE SMOKE THRESHOLD
        isDead = false; // SINCE HEALTH IS ABOVE 0 AND THE GAME JUST STARTED THE PLAYER IS NOT DEAD
    }

    // Update is called once per frame
    void Update()
    {
        //UnityEngine.Debug.Log("Health of Car is:  " + hp);

        if(hp > startPlayerHealth)
        {
            hp = startPlayerHealth; // THIS MAKES SURE THE PLAYERS HEALTH CANNOT EXCEED MAX HEALTH
        }

        if (hp > smokeThreshold)
        {
            smoke.SetActive(false); // THIS MAKES SURE IF THE PLAYERS HEALTH GOES PAST THE SMOKE THRESHOLD IT WILL TURN OFF THE SMOKE
        }

        healthBar.SetHealth(hp); // THIS MAKES THE SETHEALTH VALUE OF HEALTH BAR THE SAME AS HEALTH
    }

    private void OnCollisionEnter(Collision other)
    {
        otherOne = other;

        mag = (int)other.relativeVelocity.magnitude; // MAG BECOMES THE MAGNITUDE OF IMPACT ON COLLISION

        if (mag > damageThreshold)
        {
            hp -= mag; // IF THE MAGE IS GREATER THAN THE THRESHOLD THEN THE MAGNITUDE VALUE WILL DETRACT FROM THE HP 

            healthBar.SetHealth(hp); // UPDATES HEALTH BAR

            UnityEngine.Debug.Log("Magnitude of Impact:  " + mag);
        }

        if(hp <= smokeThreshold)
        {
            smoke.SetActive(true); // IF HP IS LESS THAN SMOKE THRESHOLD THEN TURN ON THE SMOKE
        }

        if (hp <= 0)
        {
            isDead = true; // IF HEALTH IS LESS THAN OR EQUAL TO 0 THEN THE PLAYER IS DEAD
            this.gameObject.SetActive(false); // TURNS OFF THE GAMEOBJECT PLAYER

            Instantiate(smoke, transform.position, transform.rotation); // CREATE A SMOKE PLUME AT THE CENTER OF THE EXPLOSION

            Instantiate(explosion, transform.position, transform.rotation); // CREATE A FIREBALL THE EXPANDS FROM THE CENTER OF THE PLAYERS LAST LOCATION
        }
    }
}
