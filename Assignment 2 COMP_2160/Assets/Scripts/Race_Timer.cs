﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Race_Timer : MonoBehaviour
{
    public Text timerText; //GETS THE TEXT BOX 

    private TimeSpan timerPlaying; //CREATES A TIMESPAN VALUE NAMED TIMERPLAYING

    private bool timerActivated; //CREATES A BOOL NAMED TIMERACTIVATED

    public float timer; //CREATES A FLOAT NAMED TIMER

    public string timerDisplay = ""; //CREATES A STRING NAMED TIMEDISPLAY THAT CURRENTLY DISPLAYS NOTHING
    // Start is called before the first frame update
    void Start()
    {
        timerText.text = "00:00.00"; //CREATES A FORMAT FOR THE TEXT BOX TO FOLLOW
        timerActivated = false;//THE TIMER IS NOT YET ACTIVATED
    }

    // Update is called once per frame
    void Update()
    {
        timerActivated = true; //ON UPDATE, THE TIMER BECOMES ACTIVATED
        timer = Time.timeSinceLevelLoad; // TIMER BECOMES THE TIME SINCE LEVEL LOAD 
        StartCoroutine(TimerCoroutine()); // START THE COROUTINE THAT WILL FORMAT THE TIME AND DISPLAY IT IN THE TIMER TEXT BOX
    }

    private IEnumerator TimerCoroutine()
    {
        while(timerActivated)
        {
            timerPlaying = TimeSpan.FromSeconds(timer); //TIMERPLAYING BECOMES THE TIMER VALUE FROM SECONDS UPWARD
            string timeDisplayedStr = timerPlaying.ToString("mm':'ss'.'ff"); // THIS TURNS TIMERPLAYING INTO A READABLE STRING IN THE DESIRED FORMAT
            timerText.text = timeDisplayedStr; // THE STRING IS DISPLAYED ON THE TEXT BOX

            //UnityEngine.Debug.Log(timeDisplayedStr);

            timerDisplay = timeDisplayedStr; // THIS TURNS THE EMPTY STRING INTO THE TIME DISPLAYED

            //UnityEngine.Debug.Log(timerDisplay);

            yield return null;
        }
    }
}
