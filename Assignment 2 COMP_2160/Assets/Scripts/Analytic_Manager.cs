﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;

public class Analytic_Manager : MonoBehaviour
{
    public Transform player;
    public Health_Player playerHealth;
    public Race_Timer raceTimer;
    public V2Checkpoint_Manager CPManager;

    // Start is called before the first frame update
    void Start()
    {
        AnalyticsEvent.GameStart();
    }

    // Update is called once per frame
    void Update()
    {
        if(playerHealth.isDead == true)
        {
            Dictionary<string, object> data = new Dictionary<string, object>()
            {
                {"Time", raceTimer.timerDisplay},
                {"Player_Position", player.position},
                {"Collision_Name", playerHealth.otherOne.gameObject.name}
            };

            AnalyticsEvent.GameOver("Player_Dead", data);
        }

        if(CPManager.trigger == true)
        {
            Dictionary<string, object> data = new Dictionary<string, object>()
            {
                {"time", CPManager.time[CPManager.checkpointIndex]},
                {"Player_Health", CPManager.player.GetComponent<Health_Player>().hp.ToString()}
            };

            Analytics.CustomEvent("Checkpoint", data);
        }
    }
}
