﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using UnityEngine.SceneManagement;

public class Restart : MonoBehaviour
{
    public void RestartGame()
    {
        Debug.Log("Restart");
        SceneManager.LoadScene("DrivePrototype"); //GETS UNITYENGINE SCENE MANAGEMENT AND USES IT TO RESTART THE SCENE WITH THE NAME DRIVEPROTOTYPE
    }
}
