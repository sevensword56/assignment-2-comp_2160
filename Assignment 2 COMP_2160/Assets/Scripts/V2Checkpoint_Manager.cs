﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class V2Checkpoint_Manager : MonoBehaviour
{
    //CHECKPOINTS//
    public Transform[] checkpoints; // CREATES AN EMPTY ARRAY FOR CHECKPOINTS
    public int checkpointIndex; // PROVIDES A INTEGER REPRESENTATION OF THE CHECKPOINT INDEX
    public float lightIntensity; // PROVIDES A PUBLIC OPTION FOR USERS TO CHANGE LIGHT INTENSITY
    public GameObject endGamePanel;
    public Text youWin;
    //CHECKPOINTS//
    //TIME//
    public GameObject player; // ALLOWS THIS SCRIPT TO GET COMPONENTS OFF OF THE PLAYER
    public string[] time; // CREATES AN EMPTY ARRAY FOR TIME VALUES
    private string timeItem; // PROVIDES A STRING VALUE THAT ALLOWS THE SCRIPT TO GRAB THE TIMER DISPLAY OFF OF PLAYER
    //TIME//
    //DISPLAYTIMES
    public Text checkpointText; //GETS THE TEXT BOX
    private string displayTime = ""; //PROVIDES A STRNG FOR THE TEXT BOX
    public bool trigger; //GETS THE TRIGGER FROM THE CHECKPOINT
    private float death = 0.0f; //TURNS DEATH INTO A VALUE
    //DISPLAYTIMES

    // Start is called before the first frame update
    void Start()
    {
        int size = checkpoints.Length - 1; //MODIFIES SIZE OF TEXT BOX TO SHOW THE NEEDED TEXT
        RectTransform rt = checkpointText.GetComponent<RectTransform>(); //GETS THE TRANSFORM OF THE TEXT BOX
        rt.sizeDelta = new Vector2(944, 38 * size); //MODIFIES THE TRANSFORM OF THE TEXT BOX
        checkpointIndex = 0; //START CHECKPOINT INDEX FROM 0
        time = new string[checkpoints.Length]; // MAKE THE TIME ARRAY THE SAME SIZE AS THE CHECKPOINTS ARRAY
        //ADDS INCOMPLETE TO EACH DISPLAY ITEM
        for (int i = 0; i < time.Length; i++)
        {
            time[i] = "Checkpoint Number: " + i + "  Reached at: Incomplete"; //EACH ARRAY ITEM OF TIME IS IN THIS TEMPLATE
        }
        //ADDS INCOMPLETE TO EACH DISPLAY ITEM
    }

    // Update is called once per frame
    void Update()
    {
        // ENABLES END GAME PANEL ONCE PLAYER HAS PASSED THROUGH ALL CHECKPOINTS
        if (checkpointIndex >= checkpoints.Length || player.GetComponent<Health_Player>().hp <= death)
        {
            // ADDS TIME ARRAY ONTO CHECKPOINTS ENDSCREEN
            foreach (string timestr in time)
            {
                string timeString = timestr.ToString(); // MAKES TIMESTRING A STRING REPRESENTING EACH TIME ITEM IN THE TIME ARRAY

                displayTime = displayTime.ToString() + timeString + "\n"; // DISPLAYTIME BECOMES EACH TIMESTRING ON EACH NEW LINE
            }
            // ADDS TIME ARRAY ONTO CHECKPOINTS ENDSCREEN

            endGamePanel.SetActive(true); //ENABLES END GAME PANEL ON WIN
            if(checkpointIndex >= checkpoints.Length)
            {
                youWin.text = "You Win!"; // SWITCHES TEXT TO YOU WIN
            }

            if(player.GetComponent<Health_Player>().hp <= death)
            {
                youWin.text = "You Loose!"; // SWITCHES TEXT TO YOU LOOSE
            }
            checkpointText.text = displayTime; // DISPLAYS CHECKPOINT TIMES 

        }

        // ENABLES END GAME PANEL ONCE PLAYER HAS PASSED THROUGH ALL CHECKPOINTS
        ApplyEffects(checkpointIndex);

        AddTimeToArray(checkpointIndex); // ADDS INDEX TO ITEM ARRAY AS SOON AS THE CHECKPOINT INDEX CHANGES IT ADDS THE TIME TO THE NEXT INDEX
    }

    void AddTimeToArray(int index)
    {
        if(trigger == true)
        {
            timeItem = player.GetComponent<Race_Timer>().timerDisplay; // RIPS TIMEDISPLAY STRING FROM PLAYER

            time[index] = "Checkpoint Number: " + index + "  Reached at: " +  timeItem; // ADDS TIME STRING TO TIME ARRAY IN THE FORMAT 
        }
    }

    void ApplyEffects(int index)
    {
        checkpoints[index].GetComponent<Renderer>().material.color = Color.green; // CHANGES MATERIAL COLOR TO GREEN FOR ITEM LINKED TO CHECKPOINT INDEX
        checkpoints[index].GetComponent<Light>().intensity = lightIntensity; // ADDS LIGHT INTENSITY VALUE FOR ITEM LINKED TO CHECKPOINT INDEX
        checkpoints[index].GetComponent<Collider>().enabled = true; // ENABLES THE COLLIDER FOR THE ITEM TO BE INTERACTABLE WITH PLAYER
    }
}
