﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Health_Bar : MonoBehaviour
{
    public Slider slider; // CREATES A SLIDER

    public Gradient gradient; // CREATES A GRADIENT

    public Image fill; // GRABS THE IMAGE THAT WILL BE USED AS A HEALTH BAR FILL

    public void SetMaxHealth(float health)
    {
        slider.maxValue = health; // THE SILDER MAX VALUE REPRESENTS THE HEALTH MAX VALUE
        slider.value = health; // THE SLIDERS CURRET VALUE IS ALSO THE HEALTHS CURRENT VALUE

        fill.color = gradient.Evaluate(1f);
    }

    public void SetHealth(float health)
    {
        slider.value = health; // THE SLIDERS CURRET VALUE IS ALSO THE HEALTHS CURRENT VALUE THAT GETS UPDATED

        fill.color = gradient.Evaluate(slider.normalizedValue); //THE SLIDERS CURRET VALUE IS ALSO THE HEALTHS CURRENT VALUE AND THAT IS NORMALIZED FOR THE GRADIENT
    }
}
