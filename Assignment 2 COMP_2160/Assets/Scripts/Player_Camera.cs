﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Player_Camera : MonoBehaviour
{
    public float maxSpeed = 5.0f;
    public float acceleration = 0.3f;
    public float deacceleration = 0.2f;
    public float rotationSpeed = 5.0f;
    public Transform camera;
    public float maxCameraZDistance = 2.0f;
    public float cameraRotationSpeed = 1.0f;
    public float cameraRestSpeed = 1.0f;
    public float maxCameraXDistance = 1.0f;
    public LayerMask groundLayer;

    private float speed = 0.0f;
    private Vector3 cameraRestPosition;
    private Vector3 cameraMaxPosition;
    private bool maxIsSet = false;
    private Vector3 cameraRotationVector;
    private bool grounded = false;
    private float heightOffset = 0.1f;
    private float groundedHeight = 0.25f;

    // Start is called before the first frame update
    void Start()
    {
        maxCameraZDistance = (maxCameraZDistance * -1) + camera.localPosition.z;
        cameraRestPosition = camera.localPosition;
        cameraRotationVector = camera.localEulerAngles;
    }

    // Update is called once per frame
    void Update()
    {
        isGrounded();

        if (speed <= maxSpeed && Input.GetAxis("Vertical") > 0 && grounded == true)
        {
            speed += acceleration;
        }
        else if (speed >= (maxSpeed * -1) && Input.GetAxis("Vertical") < 0 && grounded == true)
        {
            speed -= acceleration;
        }
        else if (speed >= 0)
        {
            speed -= deacceleration;
        }
        else if (speed <= 0)
        {
            speed += deacceleration;
        }
        
        float translation = speed * Time.deltaTime;
        float rotation = 0;

        transform.Translate(0,0,translation);
        if(translation != 0 && grounded == true)
        {
            rotation = (Input.GetAxis("Horizontal") * rotationSpeed) * Time.deltaTime;
            transform.Rotate(0,rotation,0);
        }

        CameraControl(rotationSpeed);
    }

    void isGrounded()
    {
        if (Physics.Raycast(new Vector3(transform.position.x, transform.position.y + heightOffset, transform.position.z),
                             Vector3.down, groundedHeight + heightOffset))
         {
            grounded = true;
         }
         else 
         {
            grounded = false;
         }
    }

    void CameraControl(float speed)
    {
        float zMovement = 0.0f;
        float xMovement = 0.0f;
        
        // Camera Vertical Movement 

        if (camera.localPosition.z >= maxCameraZDistance && camera.localPosition.z <= cameraRestPosition.z && Input.GetAxis("Vertical") != 0)
        {                                                                                                  
            zMovement = ((Input.GetAxis("Vertical") * speed) * Time.deltaTime) * -1;                                           
        } 

        // Camera Turning Movement 

        if(camera.localPosition.x <= maxCameraXDistance && camera.localPosition.x >= (maxCameraXDistance * -1) && Input.GetAxis("Horizontal") != 0)
        {
            xMovement =  (Input.GetAxis("Horizontal") * cameraRotationSpeed) * Time.deltaTime;
        }

        // Rest position X 
        if(Input.GetAxis("Horizontal") == 0 && camera.localPosition.x < -0.5)
        {
           xMovement = cameraRotationSpeed * Time.deltaTime;
        }
        if(Input.GetAxis("Horizontal") == 0 && camera.localPosition.x > 0.5)
        {
            xMovement = (cameraRotationSpeed * Time.deltaTime) * -1;
        }

        // Rest position Z
        if(Input.GetAxis("Vertical") == 0 && camera.localPosition.z >= maxCameraZDistance)
        {
            zMovement = (cameraRestSpeed * Time.deltaTime) * -1;
        }
        if(Input.GetAxis("Vertical") == 0 && camera.localPosition.z <= cameraRestPosition.z)
        {
            zMovement = cameraRestSpeed * Time.deltaTime;
        }

        camera.Translate(xMovement,0,zMovement);

        // Cancel Player Rotation // Opposite Euler angles does not work

        Vector3 rotationVector = transform.rotation.eulerAngles;
        float rotationZ = (rotationVector.z * -1);
        rotationVector = new Vector3(cameraRotationVector.x, cameraRotationVector.y, rotationZ);
        camera.localEulerAngles = rotationVector;

        // Outside bounds constraints

        if (camera.localPosition.z <= maxCameraZDistance)                                              // <- If camera position goes outside max distance
        {
            if(maxIsSet == false)
            {
                cameraMaxPosition = camera.localPosition;                                                  // <- Set max position
                maxIsSet = true;
            }
            
            camera.localPosition = new Vector3(camera.localPosition.x, cameraMaxPosition.y, maxCameraZDistance);// <- Return to max position, with z at max distance
        }
        if(camera.localPosition.z >= cameraRestPosition.z)                                                  // <- If camera gets closer then position at rest
        {
            camera.localPosition = new Vector3(camera.localPosition.x, cameraRestPosition.y, cameraRestPosition.z);                                                      // <- Set camera to starting position
        }
        if(camera.localPosition.x >= maxCameraXDistance)
        {
            camera.localPosition = new Vector3(maxCameraXDistance, camera.localPosition.y, camera.localPosition.z);
        }
        if(camera.localPosition.x <= (maxCameraXDistance * -1))
        {
            camera.localPosition = new Vector3((maxCameraXDistance * -1), camera.localPosition.y, camera.localPosition.z);
        }
        if((camera.localPosition.x < 0.5 && camera.localPosition.x > 0 && Input.GetAxis("Horizontal") == 0) ||
            (camera.localPosition.x > -0.5 && camera.localPosition.x < 0 && Input.GetAxis("Horizontal") == 0))
        {
            camera.localPosition = new Vector3(0, camera.localPosition.y, camera.localPosition.z);
        }
        
    }
}
