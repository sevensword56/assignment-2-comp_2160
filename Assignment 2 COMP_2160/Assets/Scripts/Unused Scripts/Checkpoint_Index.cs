﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Checkpoint_Index : MonoBehaviour
{
    public int checkPointIndex;

    public  GameObject checkpoint;

    public GameObject player;

    public Text checkpointTimes;

    public Text youWin;

    public float colorIntensity = 1.0f;

    public Transform[] checkpoints;

    public float[] time;

    public float timeItem;

    public float maxIndex;

    public string displayTimes = "";

    //private string incomplete = "Incomplete: ";

    private string timeString = "";

    //private string displayCheckPoints = "";

    private float playerHp;

    // Start is called before the first frame update
    void Start()
    {
        checkPointIndex = 0;

        time = new float[checkpoints.Length];

        //checkpoints[0].GetComponent<Renderer>().material.color = Color.green;
        //checkpoints[0].GetComponent<Light>().intensity = colorIntensity;
        //checkpoints[0].GetComponent<Collider>().enabled = true;
    }

    // Update is called once per frame
    void Update()
    {
        //UnityEngine.Debug.Log("Index of checkpoint: " + checkPointIndex);

        playerHp = player.GetComponent<Health_Player>().hp;

        //UnityEngine.Debug.Log("playerHp from Checkpoint_index is: " + playerHp);

        maxIndex = checkpoints.Length;
        //UnityEngine.Debug.Log("max index: " + maxIndex);

        foreach (float timetime in time)
        {
            var index = Array.IndexOf(time, timetime);

            timeString = timetime.ToString();

            if(playerHp <= 0 || checkPointIndex >= maxIndex)
            {
                displayTimes = displayTimes.ToString() + "Checkpoint Number: " + index.ToString() + "  Reached at: " + timeString + "\n"; // this works DON'T DELETE
            }

            //if (playerHp <= 0 && time[index] <= 0.0f)
            //{
            //    timeString = incomplete;
            //    displayTimes = displayTimes.ToString() + "Checkpoint Number: " + index.ToString() + "  Reached at: " + timeString + "\n";
            //}

            //if (playerHp <= 0 && time[index] > 0.0f)
            //{
            //    timeString = timetime.ToString();
            //    displayTimes = displayTimes.ToString() + "Checkpoint Number: " + index.ToString() + "  Reached at: " + timeString + "\n";
            //}
        }

        if (checkPointIndex >= maxIndex)
        {
            youWin.text = "You Win!";
            checkpointTimes.text = displayTimes;
        }

        if (playerHp <= 0.0f)
        {
            youWin.text = "You Loose";
            checkpointTimes.text = displayTimes;
        }

        checkpoints[checkPointIndex].GetComponent<Renderer>().material.color = Color.green;
        checkpoints[checkPointIndex].GetComponent<Light>().intensity = colorIntensity;
        checkpoints[checkPointIndex].GetComponent<Collider>().enabled = true;
        //checkpoints[checkPointIndex].GetComponent<Collider>().isTrigger = true;

        CheckPointTime(checkPointIndex);
    }

    void CheckPointTime(int index)
    {
        timeItem = checkpoint.GetComponent<Checkpoint>().time;

        time[index] = timeItem;
    }
}
