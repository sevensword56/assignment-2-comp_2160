﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class Checkpoint : MonoBehaviour
{
    public int index;

    public GameObject holder;

    public GameObject player;

    public Collider collider;

    public float time;

    public float healthBoost;

    private float playerHp;

    private float maxPlayerHp;

    //public bool hitDetected = false;

    // Start is called before the first frame update
    void Start()
    {
        index = 0;

        maxPlayerHp = player.GetComponent<Health_Player>().startPlayerHealth;

        //UnityEngine.Debug.Log("start HP from checkpoint: " + maxPlayerHp);
    }

    // Update is called once per frame
    void Update()
    {
        time = Time.timeSinceLevelLoad;

        playerHp = player.GetComponent<Health_Player>().hp;

        //UnityEngine.Debug.Log("player HP from checkpoint: " + playerHp);
    }

    private void OnTriggerEnter(Collider other)
    {
        
        if (other.tag == "Player")
        {
            holder.GetComponent<Checkpoint_Index>().checkPointIndex++;

            index = holder.GetComponent<Checkpoint_Index>().checkPointIndex;
        }

    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            this.GetComponent<Renderer>().material.color = Color.red;
            this.GetComponent<Light>().intensity = 0.0f;
            collider = this.GetComponent<Collider>();
            collider.enabled = !collider.enabled;

            if(playerHp < maxPlayerHp)
            {
                playerHp += healthBoost;
                player.GetComponent<Health_Player>().hp += healthBoost;
                player.GetComponent<Health_Player>().healthBar.SetHealth(playerHp);
            }

            //if(playerHp > maxPlayerHp)
            //{
            //    player.GetComponent<Health_Player>().hp = maxPlayerHp;
            //}

            //if(playerHp > player.GetComponent<Health_Player>().smokeThreshold)
            //{
            //    player.GetComponent<Health_Player>().smoke.SetActive(false);
            //}
        }
    }

}
